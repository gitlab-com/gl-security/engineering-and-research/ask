# Ask Changelog

## v0.2
- [FEATURE] Add reports validation with GitLab Schemas

## v0.1
- Initial import
