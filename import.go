package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"sigs.k8s.io/yaml"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

// Import unmarshals yaml content to Vulnerabilities in the provided report
func Import(globs []string, report *issue.Report) error {

	if len(globs) == 0 {
		globs = []string{"*.yml"}
	}

	// if filesFound is still 0 at the end, we haven't found anything to import
	var filesFound int

	for _, pattern := range globs {
		vulns := []issue.Issue{}
		files, err := filepath.Glob(pattern)
		if err != nil {
			return err
		}

		filesFound += len(files)

		for _, f := range files {
			b, err := ioutil.ReadFile(filepath.Clean(f))
			if err != nil {
				return err
			}

			err = yaml.Unmarshal(b, &vulns)
			if err != nil {
				return err
			}
			report.Vulnerabilities = append(report.Vulnerabilities, vulns...)
		}
	}

	if filesFound == 0 {
		return fmt.Errorf("Couldn't find any file matching '%s'", globs)
	}

	addScanner(report)
	report.Dedupe()
	report.Sort()

	return nil
}

func addScanner(report *issue.Report) {
	scanner := filepath.Base(os.Args[0])
	for k := range report.Vulnerabilities {
		v := &report.Vulnerabilities[k]
		if v.Scanner.ID == "" {
			v.Scanner.ID = scanner
		}
		if v.Scanner.Name == "" {
			v.Scanner.Name = scanner
		}
	}
}
