module gitlab-com/gl-security/engineering-and-research/ask

go 1.15

require (
	github.com/kylelemons/godebug v1.1.0
	github.com/sirupsen/logrus v1.4.2
	github.com/urfave/cli/v2 v2.2.0
	github.com/xeipuuv/gojsonschema v1.2.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.16.0
	golang.org/x/sys v0.0.0-20200909081042-eff7692f9009 // indirect
	sigs.k8s.io/yaml v1.2.0
)
