# ASK

Ask, the AppSec Swiss Army Knife, is a tool used by the GitLab AppSec team to interact with Security Reports.

## Deprecation notice

This project has been created to let AppSec import vulnerabilities in bulk in the Vulnerability Reports, to make them the SSOT.  
Since then, adding vulnerabilities via the API has been [released in GitLab 14.4](https://about.gitlab.com/releases/2021/10/22/gitlab-14-4-released/#directly-create-vulnerability-records-via-api).

## Usage

The best way to use this software is to use the provided Docker image:

```sh
$ docker run -it --rm registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/ask -h
```


### Import

To generate a report from yaml files:

```sh
docker run -it --rm -v $PWD:/tmp/vulns registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/ask import vulnerabilities /tmp/vulns/\*.yml
```

### Validate

To validate an existing report:

```sh
docker run -it --rm -v $PWD:/tmp/reports registry.gitlab.com/gitlab-com/gl-security/engineering-and-research/ask validate reports /tmp/reports/\*.json
```

## Contribute

See the [CONTRIBUTING.md](CONTRIBUTING.md) file.
