package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/xeipuuv/gojsonschema"
	"sigs.k8s.io/yaml"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const securityReportsSchemasURL = "https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/raw/master/dist/%s-report-format.json"

// Validate validates a report against the GitLab JSON schemas
func Validate(report *issue.Report) error {
	schemaLoader := gojsonschema.NewReferenceLoader(fmt.Sprintf(securityReportsSchemasURL, report.Scan.Type))
	documentLoader := gojsonschema.NewGoLoader(report)
	log.Infof("Validating against %s", fmt.Sprintf(securityReportsSchemasURL, report.Scan.Type))

	result, err := gojsonschema.Validate(schemaLoader, documentLoader)
	if err != nil {
		return err
	}

	if !result.Valid() {
		log.Error("The report is not valid:\n")
		for _, desc := range result.Errors() {
			log.Errorf("- %s\n", desc)
		}
		return errors.New("invalid document")
	}
	return nil
}

// ValidateFiles validates all files matching globs
func ValidateFiles(globs []string, report *issue.Report, format string) error {
	if len(globs) == 0 {
		globs = []string{"*.yml"}
	}

	var dest interface{}
	dest = &report
	if report != nil {
		dest = &report.Vulnerabilities
	}

	if globs[0] == "-" && len(globs) == 1 {
		b, err := ioutil.ReadAll(os.Stdin)
		if err != nil {
			return err
		}

		switch format {
		case "json":
			err = json.Unmarshal(b, &dest)
		case "yaml":
			err = json.Unmarshal(b, &dest)
			addScanner(report)
		default:
			return fmt.Errorf("Invalid format: %s (should be 'json' or 'yaml')", format)
		}
		if err != nil {
			return err
		}

		return Validate(report)
	}

	// if filesFound is still 0 at the end, we haven't found anything to import
	var filesFound int

	for _, pattern := range globs {
		files, err := filepath.Glob(pattern)
		if err != nil {
			return err
		}

		filesFound += len(files)

		for _, f := range files {
			b, err := ioutil.ReadFile(filepath.Clean(f))
			if err != nil {
				return err
			}

			extension := filepath.Ext(f)
			switch extension {
			case ".json":
				err = json.Unmarshal(b, &dest)
			case ".yaml", ".yml":
				err = yaml.Unmarshal(b, &dest)
				addScanner(report)
			default:
				return fmt.Errorf("Invalid file format: %s (should be 'json' or 'yaml')", extension)
			}
			if err != nil {
				return err
			}

			if err := Validate(report); err != nil {
				return fmt.Errorf("Error on file %s: %s", f, err)
			}
		}
	}

	if filesFound == 0 {
		return fmt.Errorf("Couldn't find any file matching '%s'", globs)
	}
	return nil
}
