package main

import (
	"flag"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

func TestValidate(t *testing.T) {
	log.SetOutput(ioutil.Discard)
	t.Run("Invalid report", func(t *testing.T) {
		content := `---
- category: sast
  name: vuln1
  message: Message
  description: a long description
  cve: todo
  severity: high
  location:
    file: ./main.go
    start_line: 18
    end_line: 18

- category: sast
  name: vuln2
  message: Message 2 
  description: a long description again
  cve: todo
  severity: low
  location:
    file: ./main.go
    start_line: 12
    end_line: 12`

		dir, err := ioutil.TempDir("", "example")
		if err != nil {
			log.Fatal(err)
		}
		defer os.RemoveAll(dir)

		fp := filepath.Join(dir, "vulnerabilities.yml")

		if err := ioutil.WriteFile(fp, []byte(content), 0666); err != nil {
			log.Fatal(err)
		}

		app := NewApp()
		set := flag.NewFlagSet("test", 0)
		test := []string{"validate", fp}
		_ = set.Parse(test)
		context := cli.NewContext(app, set, nil)

		report := newReport(context)
		report.Scan.Type = issue.CategorySast
		err = ValidateFiles([]string{fp}, report, "yaml")
		if err == nil {
			t.Fatal("Files should not be valid")
		}

	})

	t.Run("Valid report", func(t *testing.T) {
		content := `---
- category: sast
  name: vuln1
  message: Message
  description: a long description
  cve: todo
  identifiers:
    - type: cve
      name: CVE-2018-1234
      value: CVE-2018-1234
      url: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234
  severity: high
  location:
    file: ./main.go
    start_line: 18
    end_line: 18

- category: sast
  name: vuln2
  message: Message 2 
  description: a long description again
  cve: todo
  identifiers:
    - type: cve
      name: CVE-2018-1234
      value: CVE-2018-1234
      url: https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-1234
  severity: low
  location:
    file: ./main.go
    start_line: 12
    end_line: 12`

		dir, err := ioutil.TempDir("", "example")
		if err != nil {
			log.Fatal(err)
		}
		defer os.RemoveAll(dir)

		fp := filepath.Join(dir, "vulnerabilities.yml")

		if err := ioutil.WriteFile(fp, []byte(content), 0666); err != nil {
			log.Fatal(err)
		}

		app := NewApp()

		set := flag.NewFlagSet("test", 0)
		test := []string{"validate", fp, "-t", "sast"}
		_ = set.Parse(test)
		context := cli.NewContext(app, set, nil)

		report := newReport(context)
		report.Scan.Type = issue.CategorySast
		err = ValidateFiles([]string{fp}, report, "yaml")
		if err != nil {
			t.Fatalf("Expected nil, got error: %s", err)
		}

	})
}
