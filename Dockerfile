ARG GO_VERSION=1.15
FROM golang:$GO_VERSION-alpine AS builder

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /src

COPY . .
RUN go build -v -ldflags="-s -w" -o /bin/ask

FROM busybox
COPY --from=builder /bin/ask /usr/local/bin/
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/

ENTRYPOINT ["/usr/local/bin/ask"]
