package main

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"

	"github.com/urfave/cli/v2"
)

func main() {
	err := NewApp().Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

// NewApp generates a new cli app
func NewApp() *cli.App {
	return &cli.App{
		Name:    filepath.Base(os.Args[0]),
		Usage:   "The GitLab AppSec Swiss Army Knife",
		Version: "0.1",
		Commands: []*cli.Command{
			{
				Name:    "import",
				Usage:   "Import raw data into reports",
				Aliases: []string{"i"},
				Subcommands: []*cli.Command{
					{
						Name:      "vulnerabilities",
						Aliases:   []string{"v"},
						Usage:     "import vulnerabilities from yaml files",
						UsageText: os.Args[0] + " import vulnerabilities [*.yml]",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:    "report_type",
								Aliases: []string{"t"},
								Value:   string(issue.CategorySast),
								Usage:   "Type of report (container_scanning, dependency_scanning, dast, sast, secret_detection, coverage_fuzzing)",
							},
						},
						Action: func(c *cli.Context) error {
							globs := c.Args().Slice()

							report := newReport(c)

							err := Import(globs, report)
							if err != nil {
								return err
							}

							b, err := json.MarshalIndent(report, "", "  ")
							if err != nil {
								return err
							}
							fmt.Println(string(b))

							return nil
						},
					},
				},
			},
			{
				Name:    "validate",
				Usage:   "Validate generated reports",
				Aliases: []string{"v"},
				Subcommands: []*cli.Command{
					{
						Name:      "vulnerabilities",
						Usage:     "validate vulnerabilities from yaml files",
						UsageText: os.Args[0] + " validate vulnerabilities [*.yml]",
						Aliases:   []string{"v"},
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:    "report_type",
								Aliases: []string{"t"},
								Value:   string(issue.CategorySast),
								Usage:   "Type of report (container_scanning, dependency_scanning, dast, sast, secret_detection, coverage_fuzzing)",
							},
						},
						Action: func(c *cli.Context) error {
							globs := c.Args().Slice()

							err := ValidateFiles(globs, newReport(c), "yaml")
							if err != nil {
								return err
							}

							log.Info("Documents are valid")
							return nil
						},
					},
					{
						Name:      "reports",
						Usage:     "validate reports",
						Aliases:   []string{"r"},
						UsageText: os.Args[0] + " validate reports [*.yml]",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:    "format",
								Aliases: []string{"f"},
								Value:   "json",
								Usage:   "Report format if stdin (\"-\") used ('json' or 'yaml')",
							},
						},
						Action: func(c *cli.Context) error {
							globs := c.Args().Slice()
							err := ValidateFiles(globs, nil, c.String("format"))
							if err != nil {
								return err
							}

							log.Info("Documents are valid")
							return nil
						},
					},
				},
			},
		},
	}
}

// newReport creates a new report based on the App Context
func newReport(c *cli.Context) *issue.Report {
	report := issue.NewReport()
	t := issue.ScanTime(time.Now())
	p := &t
	report.Scan = issue.Scan{
		StartTime: p,
		EndTime:   p,
		Type:      issue.Category(c.String("report_type")),
		Scanner: issue.ScannerDetails{
			ID:      filepath.Base(os.Args[0]),
			Name:    c.App.Name,
			Version: c.App.Version,
			Vendor: issue.Vendor{
				Name: "GitLab",
			},
		},
		Status: "success",
	}
	return &report
}
